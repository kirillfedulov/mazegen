#!/bin/sh

set -xe

cflags="-O0 -Wall -Wextra -std=c99 -ggdb $(pkg-config --cflags sdl2)"
ldflags="$(pkg-config --libs sdl2)"

clang $cflags -o mazegen mazegen.c $ldflags
