#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

typedef unsigned int  uint;
typedef unsigned char byte;

#include <SDL2/SDL.h>

enum {
	ROWS = 17,
	COLS = 31,

	HAS_LEFT_WALL   = 1 << 7,
	HAS_BOTTOM_WALL = 1 << 6,
	HAS_TOP_WALL    = 1 << 5,
	HAS_RIGHT_WALL  = 1 << 4,

	CAME_FROM_LEFT   = 1 << 3,
	CAME_FROM_BOTTOM = 1 << 2,
	CAME_FROM_TOP    = 1 << 1,
	CAME_FROM_RIGHT  = 1 << 0,

	CAME_FROM_MASK   = 0x0F
};

#define DIDNT_SEE_CELL(c) (((c) & CAME_FROM_MASK) == 0)

byte cells[ROWS][COLS];

int main(void) {
	int i, j;

	srand(time(NULL));

	SDL_Init(SDL_INIT_EVERYTHING);
	int window_width  = 1280;
	int window_height = 720;
	SDL_Window *window = SDL_CreateWindow("Maze", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, window_width, window_height, 0);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

	for (i = 0; i < ROWS; i++) {
		for (j = 0; j < COLS; j++) {
			cells[i][j] = 0;
		}
	}

	for (i = 0; i < ROWS; i++) {
		cells[i][0]      |= HAS_LEFT_WALL;
		cells[i][COLS-1] |= HAS_RIGHT_WALL;
	}
	for (j = 0; j < COLS; j++) {
		cells[0][j]      |= HAS_TOP_WALL;
		cells[ROWS-1][j] |= HAS_BOTTOM_WALL;
	}

	i = 1;
	j = 0;
	cells[i][j] &= ~HAS_LEFT_WALL; /* entry */
	cells[i][j] |= CAME_FROM_LEFT;

	cells[ROWS-2][COLS-1] &= ~HAS_RIGHT_WALL; /* exit */

	bool running = true;
	bool backtrack = false;
	bool tick = true;
	while (running) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT) {
				running = false;
			}
		}

		if (backtrack && i == 1 && j == 0) {
			tick = false;
			cells[ROWS-2][COLS-1] &= ~HAS_RIGHT_WALL; /* exit */
		}

		if (tick) {
			byte cell = cells[i][j];
			if (cell & CAME_FROM_LEFT) {
				int choice = rand() & 0x3;
				if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j]) && choice == 2) { /* move top */
					if (backtrack) {
						cells[i][j] &= ~HAS_TOP_WALL;
						cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_BOTTOM_WALL | HAS_RIGHT_WALL;
					}
					cells[i - 1][j] |= CAME_FROM_BOTTOM;
					i--;
				} else if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1]) && choice == 3) { /* move right */
					if (backtrack) {
						cells[i][j] &= ~HAS_RIGHT_WALL;
						cells[i][j + 1] &= ~HAS_LEFT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_BOTTOM_WALL;
					}
					cells[i][j + 1] |= CAME_FROM_LEFT;
					j++;
				} else if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j]) && choice == 1) { /* move bottom */
					if (backtrack) {
						cells[i][j] &= ~HAS_BOTTOM_WALL;
						cells[i + 1][j] &= ~HAS_TOP_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_RIGHT_WALL;
					}
					cells[i + 1][j] |= CAME_FROM_TOP;
					i++;
				} else {
					if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j])) { /* move top */
						if (backtrack) {
							cells[i][j] &= ~HAS_TOP_WALL;
							cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_BOTTOM_WALL | HAS_RIGHT_WALL;
						}
						cells[i - 1][j] |= CAME_FROM_BOTTOM;
						i--;
					} else if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1])) { /* move right */
						if (backtrack) {
							cells[i][j] &= ~HAS_RIGHT_WALL;
							cells[i][j + 1] &= ~HAS_LEFT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_BOTTOM_WALL;
						}
						cells[i][j + 1] |= CAME_FROM_LEFT;
						j++;
					} else if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j])) { /* move bottom */
						if (backtrack) {
							cells[i][j] &= ~HAS_BOTTOM_WALL;
							cells[i + 1][j] &= ~HAS_TOP_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_RIGHT_WALL;
						}
						cells[i + 1][j] |= CAME_FROM_TOP;
						i++;
					} else { /* backtrack */
						j--;
						backtrack = true;
					}
				}
			} else if (cell & CAME_FROM_BOTTOM) {
				int choice = rand() & 0x3;
				if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1]) && choice == 0) { /* move left */
					if (backtrack) {
						cells[i][j] &= ~HAS_LEFT_WALL;
						cells[i][j - 1] &= ~HAS_RIGHT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_RIGHT_WALL;
					}
					cells[i][j - 1] |= CAME_FROM_RIGHT;
					j--;
				} else if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j]) && choice == 2) { /* move top */
					if (backtrack) {
						cells[i][j] &= ~HAS_TOP_WALL;
						cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_RIGHT_WALL | HAS_LEFT_WALL;
					}
					cells[i - 1][j] |= CAME_FROM_BOTTOM;
					i--;
				} else if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1]) && choice == 3) { /* move right */
					if (backtrack) {
						cells[i][j] &= ~HAS_RIGHT_WALL;
						cells[i][j + 1] &= ~HAS_LEFT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_LEFT_WALL;
					}
					cells[i][j + 1] |= CAME_FROM_LEFT;
					j++;
				} else {
					if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1])) { /* move left */
						if (backtrack) {
							cells[i][j] &= ~HAS_LEFT_WALL;
							cells[i][j - 1] &= ~HAS_RIGHT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_RIGHT_WALL;
						}
						cells[i][j - 1] |= CAME_FROM_RIGHT;
						j--;
					} else if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j])) { /* move top */
						if (backtrack) {
							cells[i][j] &= ~HAS_TOP_WALL;
							cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_RIGHT_WALL | HAS_LEFT_WALL;
						}
						cells[i - 1][j] |= CAME_FROM_BOTTOM;
						i--;
					} else if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1])) { /* move right */
						if (backtrack) {
							cells[i][j] &= ~HAS_RIGHT_WALL;
							cells[i][j + 1] &= ~HAS_LEFT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_LEFT_WALL;
						}
						cells[i][j + 1] |= CAME_FROM_LEFT;
						j++;
					} else { /* backtrack */
						i++;
						backtrack = true;
					}
				}
			} else if (cell & CAME_FROM_TOP) {
				int choice = rand() & 0x3;
				if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1]) && choice == 3) { /* move right */
					if (backtrack) {
						cells[i][j] &= ~HAS_RIGHT_WALL;
						cells[i][j + 1] &= ~HAS_LEFT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_BOTTOM_WALL | HAS_LEFT_WALL;
					}
					cells[i][j + 1] |= CAME_FROM_LEFT;
					j++;
				} else if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j]) && choice == 1) { /* move bottom */
					if (backtrack) {
						cells[i][j] &= ~HAS_BOTTOM_WALL;
						cells[i + 1][j] &= ~HAS_TOP_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_RIGHT_WALL | HAS_LEFT_WALL;
					}
					cells[i + 1][j] |= CAME_FROM_TOP;
					i++;
				} else if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1]) && choice == 0) { /* move left */
					if (backtrack) {
						cells[i][j] &= ~HAS_LEFT_WALL;
						cells[i][j - 1] &= ~HAS_RIGHT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_BOTTOM_WALL | HAS_RIGHT_WALL;
					}
					cells[i][j - 1] |= CAME_FROM_RIGHT;
					j--;
				} else {
					if (j + 1 < COLS && DIDNT_SEE_CELL(cells[i][j + 1])) { /* move right */
						if (backtrack) {
							cells[i][j] &= ~HAS_RIGHT_WALL;
							cells[i][j + 1] &= ~HAS_LEFT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_BOTTOM_WALL | HAS_LEFT_WALL;
						}
						cells[i][j + 1] |= CAME_FROM_LEFT;
						j++;
					} else if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j])) { /* move bottom */
						if (backtrack) {
							cells[i][j] &= ~HAS_BOTTOM_WALL;
							cells[i + 1][j] &= ~HAS_TOP_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_RIGHT_WALL | HAS_LEFT_WALL;
						}
						cells[i + 1][j] |= CAME_FROM_TOP;
						i++;
					} else if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1])) { /* move left */
						if (backtrack) {
							cells[i][j] &= ~HAS_LEFT_WALL;
							cells[i][j - 1] &= ~HAS_RIGHT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_BOTTOM_WALL | HAS_RIGHT_WALL;
						}
						cells[i][j - 1] |= CAME_FROM_RIGHT;
						j--;
					} else { /* backtrack */
						i--;
						backtrack = true;
					}
				}
			} else if (cell & CAME_FROM_RIGHT) {
				int choice = rand() & 0x3;
				if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j]) && choice == 1) { /* move bottom */
					if (backtrack) {
						cells[i][j] &= ~HAS_BOTTOM_WALL;
						cells[i + 1][j] &= ~HAS_TOP_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_LEFT_WALL;
					}
					cells[i + 1][j] |= CAME_FROM_TOP;
					i++;
				} else if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1]) && choice == 0) { /* move left */
					if (backtrack) {
						cells[i][j] &= ~HAS_LEFT_WALL;
						cells[i][j - 1] &= ~HAS_RIGHT_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_TOP_WALL | HAS_BOTTOM_WALL;
					}
					cells[i][j - 1] |= CAME_FROM_RIGHT;
					j--;
				} else if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j]) && choice == 2) { /* move top */
					if (backtrack) {
						cells[i][j] &= ~HAS_TOP_WALL;
						cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
						backtrack = false;
					} else {
						cells[i][j] |= HAS_BOTTOM_WALL | HAS_LEFT_WALL;
					}
					cells[i - 1][j] |= CAME_FROM_BOTTOM;
					i--;
				} else {
					if (i + 1 < ROWS && DIDNT_SEE_CELL(cells[i + 1][j])) { /* move bottom */
						if (backtrack) {
							cells[i][j] &= ~HAS_BOTTOM_WALL;
							cells[i + 1][j] &= ~HAS_TOP_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_LEFT_WALL;
						}
						cells[i + 1][j] |= CAME_FROM_TOP;
						i++;
					} else if (j - 1 >= 0 && DIDNT_SEE_CELL(cells[i][j - 1])) { /* move left */
						if (backtrack) {
							cells[i][j] &= ~HAS_LEFT_WALL;
							cells[i][j - 1] &= ~HAS_RIGHT_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_TOP_WALL | HAS_BOTTOM_WALL;
						}
						cells[i][j - 1] |= CAME_FROM_RIGHT;
						j--;
					} else if (i - 1 >= 0 && DIDNT_SEE_CELL(cells[i - 1][j])) { /* move top */
						if (backtrack) {
							cells[i][j] &= ~HAS_TOP_WALL;
							cells[i - 1][j] &= ~HAS_BOTTOM_WALL;
							backtrack = false;
						} else {
							cells[i][j] |= HAS_BOTTOM_WALL | HAS_LEFT_WALL;
						}
						cells[i - 1][j] |= CAME_FROM_BOTTOM;
						i--;
					} else { /* backtrack */
						j++;
						backtrack = true;
					}
				}
			}
		}

		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
		SDL_RenderClear(renderer);

		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		int margin_x = 0;
		int margin_y = 0;
		int x = 20;
		int y = 20;
		int w = 40;
		int h = 40;
		SDL_Rect rect;
		rect.x = x + j * w + w / 2;
		rect.y = y + i * h + h / 2;
		rect.w = 8;
		rect.h = 8;
		SDL_RenderFillRect(renderer, &rect);
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				byte cell = cells[i][j];
				int px = x + j * w;
				int py = y + i * h;
				if (cell & HAS_LEFT_WALL) {
					SDL_RenderDrawLine(renderer, px + margin_x, py + margin_y, px + margin_x, py + h - margin_y);
				}
				if (cell & HAS_BOTTOM_WALL) {
					SDL_RenderDrawLine(renderer, px, py + h, px + w, py + h);
				}
				if (cell & HAS_TOP_WALL) {
					SDL_RenderDrawLine(renderer, px, py, px + w, py);
				}
				if (cell & HAS_RIGHT_WALL) {
					SDL_RenderDrawLine(renderer, px + w, py, px + w, py + h);
				}
			}
		}

		SDL_RenderPresent(renderer);
		SDL_Delay(33);
	}
}
